/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/03 15:49:07 by wetieven          #+#    #+#             */
/*   Updated: 2021/06/09 14:40:06 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <signal.h>
#include "libft.h"

void	inexistent_pid(void)
{
	ft_putendl_fd("INEXISTENT PID", 1);
	exit(EXIT_FAILURE);
}

void	send_chr(pid_t srv_pid, char c)
{
	unsigned char	mask;

	mask = 0b10000000;
	while (mask)
	{
		if ((c & mask) == 0)
		{
			if (kill(srv_pid, SIGUSR1) == -1)
				return (inexistent_pid());
		}
		else
			if (kill(srv_pid, SIGUSR2) == -1)
				return (inexistent_pid());
		mask >>= 1;
		usleep(80);
	}
}

void	send_str(pid_t srv_pid, char *msg)
{
	while (*msg)
	{
		send_chr(srv_pid, *msg);
		msg++;
	}
}

void	fdbck_rcvr(int sig, siginfo_t *info, void *context)
{
	(void)sig;
	(void)context;
	ft_putnbr_fd(info->si_pid, 1);
	ft_putendl_fd(" received our message", 1);
	exit(EXIT_SUCCESS);
}

int	main(int ac, char **av)
{
	struct sigaction	fbk;
	pid_t				srv_pid;

	fbk.sa_sigaction = fdbck_rcvr;
	fbk.sa_flags = SA_SIGINFO;
	if (ac < 3)
		ft_putendl_fd("USAGE : ./client [SERVER PID] \"MESSAGE\"", 1);
	else if (ft_atoi(av[1]) <= 1)
		ft_putendl_fd("INVALID PID", 1);
	else
	{
		srv_pid = ft_atoi(av[1]);
		send_str(srv_pid, av[2]);
		send_chr(srv_pid, '\0');
		ft_putendl_fd("Message sent", 1);
	}
	sigaction(SIGUSR1, &fbk, NULL);
	while (1)
		pause();
	return (0);
}

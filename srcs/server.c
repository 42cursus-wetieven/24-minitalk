/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wetieven <wetieven@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/03 15:52:26 by wetieven          #+#    #+#             */
/*   Updated: 2021/06/09 14:42:21 by wetieven         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <signal.h>
#include "libft.h"
#include <stdio.h>

void	print_msg(t_vctr **msg, int cli_pid)
{
	vctr_push(*msg, "\n");
	write(1, (*msg)->data, (*msg)->entries);
	(*msg)->entries = 0;
	usleep(150);
	if (kill(cli_pid, SIGUSR1) == -1)
		ft_putendl_fd("CLIENT UNREACHABLE", 1);
}

void	receiver(int sig, siginfo_t *info, void *context)
{
	static unsigned char	mask = 0b10000000;
	static char				byte;
	static t_vctr			*msg;
	static int				cli_pid;

	(void)context;
	if (info->si_pid)
		cli_pid = info->si_pid;
	if (!msg)
		vctr_init(&msg, sizeof(char), 1024);
	if (sig == SIGUSR2)
		byte |= mask;
	mask >>= 1;
	if (!mask)
	{
		mask = 0b10000000;
		if (byte == '\0')
			return (print_msg(&msg, cli_pid));
		vctr_push(msg, &byte);
		byte = 0;
	}
}

int	main(void)
{
	struct sigaction	rcp;

	rcp.sa_sigaction = receiver;
	rcp.sa_flags = SA_SIGINFO;
	write(1, "Server started @ PID : ", 23);
	ft_putnbr_fd(getpid(), 1);
	write(1, "\n", 1);
	sigaction(SIGUSR1, &rcp, NULL);
	sigaction(SIGUSR2, &rcp, NULL);
	while (1)
		pause();
	return (0);
}
